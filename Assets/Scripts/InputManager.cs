using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    const int deviceNum = 0;
    WebCamTexture camTexture;

    private void Awake()
    {
        WebCamDevice[] devices = WebCamTexture.devices;
        if(devices.Length <= 0)
        {
            Debug.Log("There is no connected webcam..");
        }
        else
        {
            WebCamDevice device = devices[deviceNum];
            camTexture = new WebCamTexture(device.name);
            camTexture.Play();
        }
    }

    public Texture GetCamTexture
    {
        get { return camTexture; }
    }
    
}
